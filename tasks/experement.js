/**
 * example
 * ///import test=модуль/lib/comp
 *
 */

module.exports = function(grunt) {
    'use strict';
    var path = require('path');
    var fs = require('fs');

    //кеширование путией к модулям (исползование заремарено)
    var cache = {};
    //очередь файлов на обработку, включинные директивой модули так же попадают в этот список и тоже обрабатываются
    var files=[];

    grunt.registerMultiTask("experement", function(){
        var options = this.options({
            enabled: false
        });

        //перебираем все файлы которые передал нам grunt по маске, и скаладываем их в массив files
        this.files.forEach(function(f) {
            f.src.forEach(function(filepath) {
                if (!grunt.file.exists(filepath)) {
                    grunt.log.warn('Source file "' + filepath + '" not found.');
                } else {
                    files.push(filepath);//собираем файлы для обрабоки в очередь
                }
            });
        });

        //перебираем все файлы и обрабатываем каждый
        for (var index=0;index<files.length;index++){
            var  filePath=files[index];
            processFile(grunt.file.read(filePath),filePath);
        };

    });

    var regexpDirective=/\/\/\/import (.*=.*;)/g, //регулярка для поиска деректив
        regexpImport=/import .*=require.*\/\/\/auto generation\r\n/g, //регулярка для поиска сгенеренных import
        regexpErrorComment=/\/\/error:file .* not found.*\r\n/g; // регулярка для поиска сгенеренных строк с ошибками

    /**
     * Обрабатываем файл
     * @param data
     * @param pathToFile
     */
    var processFile=function(data,pathToFile){
        var items=data.match(regexpDirective),//найдем все дерективы
            fullpath=getAbsolutePath(pathToFile),
            newData = data;

        //удалим старый сгенерированный код
        newData=newData.replace(regexpImport, '');
        newData=newData.replace(regexpErrorComment, '');

        //если найдены дириктивы, переберем их и обработаем каждую
        if(items) {
            items.forEach(function (item) {
                var itemPath = item.split("=")[1].split(';')[0],
                    varName = item.split("=")[0].split(" ")[1],
                    fullPathToModule = findModule(fullpath, itemPath), //получим абсолютный путь до файла в модуле
                    resolvePathToModule = (fullPathToModule) ? resolvePath(fullpath, fullPathToModule) : "",
                    parts = newData.split(item);

                if (fullPathToModule) {
                    if(files.indexOf(fullPathToModule)<0) files.push(fullPathToModule);

                    //избавимся от расширения файла в пути
                    resolvePathToModule=path.dirname(resolvePathToModule)+path.basename(resolvePathToModule).replace(".d.ts",'').replace('.ts','');

                    //добавляем import в файл
                    newData = parts[0] +
                        item + "\r\n" +
                        "import " + varName + "=require('" + resolvePathToModule + "');///auto generation" +
                        parts[1];
                } else {
                    //добавляем коммент с ошибкой в файл
                    newData = parts[0] + item + "\r\n" + "//error:file " + itemPath + " not found" + parts[1];
                }
            });
        }

        //если файл изменился тогда сохраним
        if(data.length!=newData.length || data!=newData) {
            grunt.file.write(pathToFile, newData);
        }
    }

    //cache[itempath] это кеширование, но я подозреваю что такой кеш меняет логику подключения пакетов, пока заремарен

    /**
     * ищет папку node_modules а в ней файл по пути itempath
     */
    var findModule=function(startPath,modulePath){
        var fullpath="",
            patharray=startPath.split("/"),
            result="";

        //if(itempath in cache) return cache[itempath];

        while(patharray.pop()){
            fullpath=patharray.join('/')+"/node_modules/"+modulePath;

            if(grunt.file.exists(fullpath+".ts")) result=fullpath+".ts";
            if(grunt.file.exists(fullpath+".d.ts") && grunt.file.exists(fullpath+".js")) result=fullpath+".d.ts";
            if(grunt.file.exists(fullpath+"/index.ts")) result=fullpath+"/index.ts";
            if(grunt.file.exists(fullpath+"/index.d.ts") && grunt.file.exists(fullpath+"/index.js")) result=fullpath+"/index.d.ts";

            if(result){
                break;
            }
        }

        //cache[itempath]=result;
        return result;
    }

    /**
     *  возвращает обсолютный путь
     */
    var getAbsolutePath=function(targetPath){
        var fullPath=process.cwd()+"/"+targetPath;
        return fullPath.replace(/\\/g,'/');
    }

    /**
     * возвращает относительный путь от filePath до modulePath
     */
    var resolvePath=function(filePath,modulePath){

        //========== вариант 1, используя иетод библиотеки path =========//

        var resolvePath=path.relative(filePath,modulePath).split('\\');
        resolvePath.shift();
        return resolvePath.join('/');

        //================ вариант 2, ручками ===============================//
        /*
        var arraySource=filePath.split('/'),
            arrayTarget=modulePath.split('/'),
            count= 0,
            resolvePath="";

         //определяем номер последнего общего участка пути
         while(arraySource[count] && arrayTarget[count] && arraySource[count]==arrayTarget[count])count++;

         //поднимаемся на необходимое количество уровней выше, чтобы достичь общего участка пути
         for(var i=count;i<arraySource.length-1;i++){
         resolvePath+="../";
         }

         //прибавляем остаток пути от общего участка до пакета и возвращаем рузультат
         return resolvePath+arrayTarget.slice(count).join('/');
         */
    }

};