# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
* package preprocessor for typescript
* Version 0.0.1

### How it works? ###

```
#!javascript

///import test=reactts ui component/lib/ViewPort;
```

transform to (example)

```
#!javascript

///import test=reactts ui component/lib/ViewPort;
import test=require('../../node_modules/reactts ui component/libViewPort');///auto generation
```
or

```
#!javascript

///import test=reactts ui component/lib/ViewPort;
//error:file reactts ui component/lib/ViewPort not found
```
 

### grunt task example ###

```
#!javascrip

experement:{
    test: {
        outDir: 'public',
        src: 'test/**/*.ts'
    }
}
```

### support modules ###
ViewPort.ts or ViewPort.d.ts + ViewPort.js